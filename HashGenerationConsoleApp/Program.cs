﻿using HashGenerationConsoleApp.nbOperationalEntityV2;
using System;
using System.Linq;

namespace HashGenerationConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int grpid = 0;
            while(grpid != 1)
            {
                Console.WriteLine("Enter GroupId:");
                grpid = Convert.ToInt32(Console.ReadLine());
                if (grpid == 1) return;
                using (var ctx = new NewBenefitsOperational_METestingContext())
                {
                    Console.WriteLine(DateTime.Now + "  :: Started");
                    var members = ctx.Members.Where(x => x.GroupId == grpid).ToList();
                    int i = 0;
                    Console.WriteLine(DateTime.Now + "  :: Members Readed " + members.Count);
                    //var mem = ctx.Members.FirstOrDefault(m => m.MemberId == memberId);
                    foreach (var mem in members)
                    {
                        i++;
                        if (i % 10000 == 0) Console.WriteLine(DateTime.Now + "  :: 10000 done");
                        var h = mem.GenerateHashValues();
                        mem.IdentityHash = h.identityHash;
                        mem.SnapshotHash = h.snapshotHash;
                        mem.GroupMemberRefIdHash = h.GroupMemberRefIdHash;
                        //Console.WriteLine("Updated values for : " + mem.MemberId + " " + mem.FirstName + ", " + mem.LastName);
                    }
                    Console.WriteLine("Updating Members...");
                    ctx.Members.UpdateRange(members);
                    ctx.SaveChanges();
                    Console.WriteLine(DateTime.Now + "   :: Updation Completed");
                }
            }
            Console.ReadLine();
        }
    }
}
