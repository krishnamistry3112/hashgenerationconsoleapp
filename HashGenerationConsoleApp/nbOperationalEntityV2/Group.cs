﻿using System;
using System.Collections.Generic;

#nullable disable

namespace HashGenerationConsoleApp.nbOperationalEntityV2
{
    public partial class Group
    {
        public Group()
        {
            InverseCombinedInvoiceGroup = new HashSet<Group>();
        }

        public int GroupId { get; set; }
        public string GroupRefId { get; set; }
        public string GroupName { get; set; }
        public byte InvoiceCycleId { get; set; }
        public short? CategoryCodeId { get; set; }
        public int? CombinedInvoiceGroupId { get; set; }
        public byte? HostTypeId { get; set; }
        public string Host { get; set; }
        public byte? EligibilityFileDataContractId { get; set; }
        public bool PrintFulfillment { get; set; }
        public bool PrintDependentFulfillment { get; set; }
        public bool? FullAddressRequired { get; set; }
        public bool GroupSpecifiesMemberId { get; set; }
        public decimal PrePaidBalanceWarning { get; set; }
        public int? RenewalMonthId { get; set; }
        public string Website { get; set; }
        public string Creator { get; set; }
        public DateTime Created { get; set; }
        public string Modifier { get; set; }
        public DateTime? Modified { get; set; }
        public byte[] RowVersion { get; set; }
        public byte? FulfillmentShipToTypeId { get; set; }
        public int? FulfillmentShipToMarketerId { get; set; }
        public byte? GroupTypeId { get; set; }
        public short? StandardIndustrialClassificationId { get; set; }
        public byte? BillingCycleId { get; set; }
        public byte? SubscriptionTypeId { get; set; }
        public bool? SendInvoice { get; set; }
        public string CustomCompanyName { get; set; }
        public string CustomAddress1 { get; set; }
        public string CustomAddress2 { get; set; }
        public string CustomCity { get; set; }
        public int? CustomStateId { get; set; }
        public string CustomZip { get; set; }
        public int? MarketerId { get; set; }
        public byte GroupInvoiceDeliveryMethodTypeId { get; set; }
        public byte? MerchantAccountId { get; set; }

        public virtual Group CombinedInvoiceGroup { get; set; }
        public virtual ICollection<Group> InverseCombinedInvoiceGroup { get; set; }
    }
}
