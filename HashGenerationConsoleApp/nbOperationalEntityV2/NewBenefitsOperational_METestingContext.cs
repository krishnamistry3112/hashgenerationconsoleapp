﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace HashGenerationConsoleApp.nbOperationalEntityV2
{
    public partial class NewBenefitsOperational_METestingContext : DbContext
    {
        public NewBenefitsOperational_METestingContext()
        {
        }

        public NewBenefitsOperational_METestingContext(DbContextOptions<NewBenefitsOperational_METestingContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Member> Members { get; set; }
        public virtual DbSet<Group> Groups { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Data Source=nbsqld01\\d1;Integrated Security=True;Pooling=False;;Initial Catalog=NewBenefits.Operational_METesting");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<Group>(entity =>
            {
                entity.ToTable("Group", "nb");

                entity.HasIndex(e => e.CombinedInvoiceGroupId, "IX_Group_CombinedInvoiceGroupId");

                entity.HasIndex(e => e.EligibilityFileDataContractId, "IX_Group_EligibilityFileDataContractId");

                entity.HasIndex(e => e.FulfillmentShipToMarketerId, "IX_Group_FulfillmentShipToMarketerId");

                entity.HasIndex(e => e.FulfillmentShipToTypeId, "IX_Group_FulfillmentShipToTypeId");

                entity.HasIndex(e => e.GroupName, "IX_Group_GroupName");

                entity.HasIndex(e => e.GroupTypeId, "IX_Group_GroupTypeId");

                entity.HasIndex(e => e.StandardIndustrialClassificationId, "IX_Group_StandardIndustrialClassificationId");

                entity.HasIndex(e => e.InvoiceCycleId, "IX_nb.Group_InvoiceCycleId");

                entity.HasIndex(e => e.MerchantAccountId, "IX_nb_Group__MerchantAccountId");

                entity.HasIndex(e => e.RenewalMonthId, "IX_nb_Group__RenewalMonthId");

                entity.HasIndex(e => e.GroupName, "UX_GroupName")
                    .IsUnique();

                entity.HasIndex(e => e.GroupRefId, "UX_GroupRefId")
                    .IsUnique();

                entity.Property(e => e.Created).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('NewBenefits')");

                entity.Property(e => e.CustomAddress1).HasMaxLength(128);

                entity.Property(e => e.CustomAddress2).HasMaxLength(128);

                entity.Property(e => e.CustomCity).HasMaxLength(128);

                entity.Property(e => e.CustomCompanyName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CustomZip)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.FullAddressRequired)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.GroupInvoiceDeliveryMethodTypeId).HasDefaultValueSql("((1))");

                entity.Property(e => e.GroupName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.GroupRefId)
                    .IsRequired()
                    .HasMaxLength(12);

                entity.Property(e => e.Host)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceCycleId)
                    .HasDefaultValueSql("((1))")
                    .HasComment("1 = Monthly invoice cycle.  Any other number indicates a custom invoice cycle.");

                entity.Property(e => e.Modifier)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PrePaidBalanceWarning)
                    .HasColumnType("decimal(19, 4)")
                    .HasDefaultValueSql("((50.00))");

                entity.Property(e => e.RowVersion)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.Property(e => e.SendInvoice)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Website)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.HasOne(d => d.CombinedInvoiceGroup)
                    .WithMany(p => p.InverseCombinedInvoiceGroup)
                    .HasForeignKey(d => d.CombinedInvoiceGroupId)
                    .HasConstraintName("FK_Group_CombinedInvoiceGroupId");
            });


            modelBuilder.Entity<Member>(entity =>
            {
                entity.ToTable("Member", "nb");

                entity.HasIndex(e => e.MemberRefId, "IX_MemberRefId");

                entity.HasIndex(e => e.GroupId, "IX_Member_GroupId");

                entity.HasIndex(e => e.GroupMemberRefIdHash, "IX_Member_GroupMemberRefIdHash");

                entity.HasIndex(e => e.TemplateId, "IX_nb_Member__TemplateId")
                    .HasFillFactor((byte)80);

                entity.HasIndex(e => new { e.GroupId, e.MemberRefId }, "UX_Member_MemberRefId_GroupId")
                    .IsUnique();

                entity.Property(e => e.AddressHash).HasMaxLength(32);

                entity.Property(e => e.Affiliate).HasMaxLength(128);

                entity.Property(e => e.BirthDate).HasColumnType("date");

                entity.Property(e => e.Created)
                    .HasPrecision(0)
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Creator)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('NewBenefits')");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Gender)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.GroupMemberRefId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GroupMemberRefIdHash).HasMaxLength(32);

                entity.Property(e => e.IdentityHash)
                    .HasMaxLength(96)
                    .IsUnicode(false);

                entity.Property(e => e.Language)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('E')");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.MemberRefId)
                    .IsRequired()
                    .HasMaxLength(12);

                entity.Property(e => e.MemberSinceDate).HasDefaultValueSql("(getdate())");

                entity.Property(e => e.MiddleInitial)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Misc1)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc10)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc11)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc12)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc13)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc14)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc15)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc16)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc17)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc18)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc19)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc2)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc20)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc3)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc4)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc5)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc6)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc7)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc8)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Misc9)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Modified).HasPrecision(0);

                entity.Property(e => e.Modifier)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Prefix)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RowVersion)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken();

                entity.Property(e => e.SnapshotHash)
                    .HasMaxLength(96)
                    .IsUnicode(false);

                entity.Property(e => e.Ssn)
                    .HasMaxLength(11)
                    .IsUnicode(false)
                    .HasColumnName("SSN");

                entity.Property(e => e.Status)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Suffix)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.HasSequence<int>("MemberRefIds", "nb").StartsAt(100000000);

            modelBuilder.HasSequence<int>("MemberRefIds1", "nb").StartsAt(100000000);

            modelBuilder.HasSequence<int>("MemberRefIds2", "nb").StartsAt(100000000);

            modelBuilder.HasSequence<int>("MemberRefIds3", "nb").StartsAt(100000000);

            modelBuilder.HasSequence<int>("MemberRefIds4", "nb").StartsAt(100000000);

            modelBuilder.HasSequence<int>("MemberRefIds5", "nb").StartsAt(100000000);

            modelBuilder.HasSequence("MerchantCustomerId", "nb").HasMin(0);

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
