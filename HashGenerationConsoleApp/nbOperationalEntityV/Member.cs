﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

#nullable disable

namespace HashGenerationConsoleApp.nbOperationalEntityV
{
    public partial class Member
    {
        public int MemberId { get; set; }
        public string MemberRefId { get; set; }
        public string FirstName { get; set; }
        public string MiddleInitial { get; set; }
        public string LastName { get; set; }
        public int GroupId { get; set; }
        public string Gender { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Creator { get; set; }
        public DateTime Created { get; set; }
        public string Modifier { get; set; }
        public DateTime? Modified { get; set; }
        public byte[] RowVersion { get; set; }
        public int? TemplateId { get; set; }
        public string Ssn { get; set; }
        public string Misc1 { get; set; }
        public string Misc2 { get; set; }
        public string Misc3 { get; set; }
        public string Misc4 { get; set; }
        public string Misc5 { get; set; }
        public string Misc6 { get; set; }
        public string Misc7 { get; set; }
        public string Misc8 { get; set; }
        public string Misc9 { get; set; }
        public string Misc10 { get; set; }
        public string Misc11 { get; set; }
        public string Misc12 { get; set; }
        public string Misc13 { get; set; }
        public string Misc14 { get; set; }
        public string Misc15 { get; set; }
        public string Misc16 { get; set; }
        public string Misc17 { get; set; }
        public string Misc18 { get; set; }
        public string Misc19 { get; set; }
        public string Misc20 { get; set; }
        public string Affiliate { get; set; }
        public string Language { get; set; }
        public string GroupMemberRefId { get; set; }
        public string Status { get; set; }
        public string Suffix { get; set; }
        public string Prefix { get; set; }
        public DateTime MemberSinceDate { get; set; }
        public int? CollationId { get; set; }
        public string IdentityHash { get; set; }
        public string SnapshotHash { get; set; }
        public byte[] GroupMemberRefIdHash { get; set; }
        public byte[] AddressHash { get; set; }
        public bool? PrintFulfillment { get; set; }
        public int? GroupSubscriptionId { get; set; }
        public int? CountryId { get; set; }
        public int? StateId { get; set; }
        public string GroupRefId { get; set; }
        public int? MemberUcid { get; set; }

        protected string _identityPropertyValues;
        protected string _snapshotPropertyValues;
        //protected Lazy<string> _addressPropertyValues;
        protected byte[] _vantageKey;

        public Hashes GenerateHashValues()
        {
            Hashes h = new Hashes();
            _vantageKey = new byte[64];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(_vantageKey);
            }

            //_identityPropertyValues = new Lazy<string>(() => GetIdentityHashProperties());
            //_snapshotPropertyValues = new Lazy<string>(() => GetSnapshotHashProperties());
            _identityPropertyValues = string.Join(string.Empty, new String[] { FirstName, MiddleInitial, LastName, BirthDate.ToString() });
            _snapshotPropertyValues = string.Join(string.Empty, new String[]
            {
                _identityPropertyValues,
                MemberRefId,
                GroupMemberRefId,
                Gender,
                Misc1,
                Misc2,
                Misc3,
                Misc4,
                Misc5,
                Misc6,
                Misc7,
                Misc8,
                Misc9,
                Misc10,
                Misc11,
                Misc12,
                Misc13,
                Misc14,
                Misc15,
                Misc16,
                Misc17,
                Misc18,
                Misc19,
                Misc20,
                //HomePhone,
                //WorkPhone,
                //Email,
                Affiliate,
                Language,
                Suffix,
                Prefix,
                //Subscription
            });


            using (var sha384 = new SHA384Managed())
            {
                h.identityHash = string.Join("", (sha384.ComputeHash(Encoding.UTF8.GetBytes(_identityPropertyValues))).Select(x => x.ToString("x2")).ToArray());
                h.snapshotHash = string.Join("", (sha384.ComputeHash(Encoding.UTF8.GetBytes(_snapshotPropertyValues))).Select(x => x.ToString("x2")).ToArray());
            }

            using (HMACSHA256 hmac = new HMACSHA256(_vantageKey))
            {
                h.GroupMemberRefIdHash = EmptyToNull(GroupMemberRefId) == null ? null : hmac.ComputeHash(Encoding.Default.GetBytes(GroupRefId + GroupMemberRefId));
            }

            return h;
        }

        public static string EmptyToNull(string s) => string.IsNullOrWhiteSpace(s) ? null : s;

    }

    public class Hashes
    {
        public string identityHash;
        public string snapshotHash;
        public byte[] GroupMemberRefIdHash;
    }
}
